# Contextualized State
The aim of this module is to provide contextualized state to be stored into user session based. Once each user has its
own context data, the system can behavior in one different way based on contextualized state.

# Usage
To use this module is mandatory you create two classes that is the class **Context** and the class **Context Provider**.

### Dispatching events to set contexts
To dispatch events to alter the context you defined you should use event_dispatcher of the Drupal to dispatch events.
The context resolver of the contextualized_state module will try to identify which context should be set.
However, if you want to specify which context should be set you can do it passing plugin_id with the value of the context provider plugin id to the event.
For example: plugin_id: soccer_context_provider

You can use the **type** attribute to set a group of context of your interest.
```php
$event = ContextEvent::from([
    'plugin_id' => 'soccer_context_provider', // this context provider will be set.
    'type' => 'football',
    'title' => $title,
    'campaign_id' => 21,
    'campaign_brand' => 'San Paolo',
    'game_type' => 'soccer',
    'player_id' => 23,
    'game_mode' => 'for_prize',
]);

\Drupal::service('event_dispatcher')->dispatch(ContextEvent::ON_SET_CONTEXT, $event);
```

### How to get context values
To retrieve context values from a given context implementation you should call the **contextualized_state.context_manager.service** service
and then call the method getContext to retrieve your context passing the plugin_id of the context provider class as parameter.
```php
$contextManager = \Drupal::service('contextualized_state.context_manager.service');
$context = $contextManager->getContext('soccer_context_provider'); // The context provider plugin id

$context->getAll(); // All context values
$context->getState('campaign_id')->getValue(); // The context state value.
```

## Context Class
The class context is responsible for creates an abstract layer to manipulate data of a given context.
That way each context class knows just what they need.

### Creating the Context Class
You should implement a new class that inherits from src/Context/BaseContext.php class.
You can check how to implement looking on the file src/Context/ExampleContext.php

```php

/**
 * The example context.
 */
final class ExampleContext extends BaseContext {

  const STORE_ID = 'example';

  /**
   * The store id of the context.
   */
  public function getStoreId(): string {
    return self::STORE_ID;
  }

  /**
   * The context construct.
   *
   * @param array $context
   *   The context data.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temp store factory.
   */
  public function __construct(array $context, PrivateTempStoreFactory $privateTempStoreFactory) {
    parent::__construct($context, $privateTempStoreFactory);

    $this->setStates(
      State::create('data_key', $context['context_data_key'] ?? NULL),
      State::create('data_key_2', $context['context_data_key_2'] ?? NULL),
    );
  }

}
```
## Context Provider Class
The only purpose of this class is to provide the context class implementation.
This is important because it's only with the drupal plugin system we can manage and manipulate each context separately
improving the low decoupling.

### Creating the Context Provider Class
To create the Context Provider class check the **contextualized_state_examples/src/Plugin/ContextualizedState/SoccerContextProvider.php** class. 

# Available hooks
With the below hooks it's possible to alter the context and load some data before the context be set.

You can to implement **contextualized_state_alter** hook and **contextualized_state_CONTEXT_TYPE_alter** hook to alter the event context data that are being set.
For example:
```php
function my_module_contextualized_state_alter(&$contextData) {}

function my_module_contextualized_state_soccer_context_alter(&$contextData) {}
```

# Examples
Enable the module contextualized_state_examples and check how it works on the route /examples/contextualized-state/dispatch