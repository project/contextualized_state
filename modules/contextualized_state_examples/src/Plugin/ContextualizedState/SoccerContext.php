<?php

namespace Drupal\contextualized_state_examples\Plugin\ContextualizedState;

use Drupal\contextualized_state\Context\BaseContext;
use Drupal\contextualized_state\State\State;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * The soccer context.
 */
final class SoccerContext extends BaseContext {

  const STORE_ID = 'soccer';

  /**
   * {@inheritDoc}
   */
  public function getStoreId(): string {
    return self::STORE_ID;
  }

  /**
   * The soccer context construct.
   *
   * @param array $context
   *   The context data.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temp store factory.
   */
  public function __construct(array $context, PrivateTempStoreFactory $privateTempStoreFactory) {
    parent::__construct($context, $privateTempStoreFactory);

    if (empty($context)) {
      return;
    }

    $this->setStates(
      State::create('campaign_id', $context['campaign_id'] ?? NULL),
      State::create('player_id', $context['player_id'] ?? NULL),
      State::create('game_mode', $context['game_mode'] ?? 'for_fun'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags(): array {
    $campaignId = $this->getState('campaign_id');
    $playerId = $this->getState('player_id');

    return ["soccer_context:{$campaignId}:{$playerId}"];
  }

}
