<?php

namespace Drupal\contextualized_state_examples\Plugin\ContextualizedState;

use Drupal\contextualized_state\Plugin\ContextualizedStateProviderBase;
use Drupal\contextualized_state\Plugin\ContextualizedStateProviderInterface;
use Drupal\contextualized_state\Annotation\ContextualizedStateProvider;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Soccer Context provider.
 *
 * @ContextualizedStateProvider(
 *   id = "soccer_context_provider",
 *   label = @Translation("Soccer Context Provider"),
 *   supported_types = {
 *     "soccer_competition",
 *     "football"
 *   },
 * )
 */
final class SoccerContextProvider extends ContextualizedStateProviderBase implements ContextualizedStateProviderInterface {

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private PrivateTempStoreFactory $privateTempStoreFactory;

  /**
   * PredictorContextProvider constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temp store factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PrivateTempStoreFactory $privateTempStoreFactory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->privateTempStoreFactory = $privateTempStoreFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tempstore.private')
    );
  }

  /**
   * Initializes the context object.
   *
   * @param array $context
   *   The context data.
   */
  public function initializeContext(array $context = []) {
    $this->setContext(new SoccerContext($context, $this->privateTempStoreFactory));
  }

}
