<?php

namespace Drupal\contextualized_state\Service;

use Drupal\contextualized_state\Context\BaseContext;
use Drupal\contextualized_state\Plugin\ContextualizedStateProviderBase;
use Drupal\contextualized_state\Plugin\ContextualizedStateProviderManager;

/**
 * The ContextualizedStateManager service class.
 */
final class ContextualizedStateManager {

  /**
   * The context provider manager.
   *
   * @var \Drupal\contextualized_state\Plugin\ContextualizedStateProviderManager
   */
  protected ContextualizedStateProviderManager $contextProviderManager;

  /**
   * The service constructor.
   *
   * @param \Drupal\contextualized_state\Plugin\ContextualizedStateProviderManager $contextProviderManager
   *   The context provider manager.
   */
  public function __construct(
    ContextualizedStateProviderManager $contextProviderManager
  ) {
    $this->contextProviderManager = $contextProviderManager;
  }

  /**
   * Get context provider instance.
   *
   * @param string $pluginId
   *   The plugin id.
   * @param array $context
   *   The context array data.
   * @param array $configs
   *   The configs of the plugin.
   *
   * @return \Drupal\contextualized_state\Plugin\ContextualizedStateProviderBase
   *   The plugin context provider object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getContextProvider(string $pluginId, array $context = [], array $configs = []): ContextualizedStateProviderBase {
    $contextProvider = $this->contextProviderManager->createInstance($pluginId, $configs);

    $contextProvider->initializeContext($context);

    return $contextProvider;
  }

  /**
   * Helper function to shortcut the call of the context.
   *
   * @param string $pluginId
   *   The plugin id.
   * @param array $context
   *   The context data.
   * @param array $configs
   *   The configs of the plugin.
   *
   * @return \Drupal\contextualized_state\Context\BaseContext
   *   The base context implementation.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getContext(string $pluginId, array $context = [], array $configs = []): BaseContext {
    $contextProvider = $this->getContextProvider($pluginId, $context, $configs);

    return $contextProvider->getContext();
  }

  /**
   * Resolves the right context based on the event and context data.
   *
   * @param array $contextData
   *   The context data.
   *
   * @return \Drupal\contextualized_state\Context\BaseContext|null
   *   The base context.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function contextResolver(array $contextData): ?BaseContext {
    $context = NULL;

    if (!empty($contextData['plugin_id'])) {
      $context = $this->getContext($contextData['plugin_id'], $contextData);
    }

    if (empty($contextData['type'])) {
      throw new \InvalidArgumentException('The type attribute is required to set a context.');
    }

    $pluginDefinitions = $this->contextProviderManager->getDefinitions();
    foreach ($pluginDefinitions as $pluginId => $pluginDefinition) {
      $contextProvider = $this->contextProviderManager->createInstance($pluginId);

      if (!$contextProvider->isTypeSupported($contextData['type'])) {
        continue;
      }

      $contextProvider->initializeContext($contextData);

      $context = $contextProvider->getContext();
    }

    return $context;
  }

}
