<?php

namespace Drupal\contextualized_state\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Context Provider item annotation object.
 *
 * @see \Drupal\contextualized_state\Plugin\ContextualizedStateProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class ContextualizedStateProvider extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The supported context types.
   *
   * @var array
   */
  public array $supported_types = [];

}
