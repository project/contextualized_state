<?php

namespace Drupal\contextualized_state\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Represents context operations as event.
 */
class ContextEvent extends Event {

  /**
   * The set context event name.
   */
  const ON_SET_CONTEXT = 'contextualized_state.set';

  /**
   * The context data.
   *
   * @var array
   */
  public array $context;

  /**
   * The context event construct.
   */
  public function __construct(array $context) {
    $this->context = $context;
  }

  /**
   * Creates new context event.
   *
   * @param array $context
   *   The context data.
   *
   * @return \Drupal\contextualized_state\Event\ContextEvent
   *   The context object.
   */
  public static function from(array $context): ContextEvent {
    return new static($context);
  }

}
