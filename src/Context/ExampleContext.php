<?php

namespace Drupal\contextualized_state\Context;

use Drupal\contextualized_state\State\State;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * The example context.
 */
final class ExampleContext extends BaseContext {

  const STORE_ID = 'example';

  /**
   * {@inheritDoc}
   */
  public function getStoreId(): string {
    return self::STORE_ID;
  }

  /**
   * The predictor context construct.
   *
   * @param array $context
   *   The context data.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temp store factory.
   */
  public function __construct(array $context, PrivateTempStoreFactory $privateTempStoreFactory) {
    parent::__construct($context, $privateTempStoreFactory);

    $this->setStates(
      State::create('data_key', $context['context_data_key'] ?? NULL),
      State::create('data_key_2', $context['context_data_key_2'] ?? NULL),
    );
  }

}
