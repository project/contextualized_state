<?php

namespace Drupal\contextualized_state\Context;

/**
 * Context should store all states of the current moment.
 */
interface ContextInterface {

  /**
   * Gets the store key name.
   *
   * @return string
   *   The store key name.
   */
  public function getStoreKeyName(): string;

  /**
   * Gets the store states.
   *
   * @return array
   *   The state.
   */
  public function getStates(): array;

}
