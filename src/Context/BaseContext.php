<?php

namespace Drupal\contextualized_state\Context;

use Drupal\contextualized_state\State\NullState;
use Drupal\contextualized_state\State\State;
use Drupal\contextualized_state\State\StateInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Context should store all states of the current moment.
 */
abstract class BaseContext implements ContextInterface {

  /**
   * The store key name.
   */
  const STORE_KEY_NAME = 'contextualized_state';

  /**
   * The states object.
   *
   * @var array
   */
  private array $states = [];

  /**
   * The private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private PrivateTempStore $privateTempStore;

  /**
   * The predictor context construct.
   *
   * @param array $context
   *   The context data.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStoreFactory
   *   The private temp store factory.
   */
  public function __construct(array $context, PrivateTempStoreFactory $privateTempStoreFactory) {
    $this->privateTempStore = $privateTempStoreFactory->get($this->getStoreKeyName());
  }

  /**
   * Retrieve store id.
   *
   * @return string
   *   The store id.
   */
  abstract public function getStoreId(): string;

  /**
   * Gets the store key name.
   *
   * @return string
   *   The store key name.
   */
  public function getStoreKeyName(): string {
    $storeName = $this->getStoreId();

    return self::STORE_KEY_NAME . '_' . $storeName;
  }

  /**
   * Gets the states of context.
   *
   * @return array
   *   The states list.
   */
  public function getStates(): array {
    return $this->states;
  }

  /**
   * Checks whether the context has state or not.
   *
   * @param string $key
   *   The key state.
   *
   * @return bool
   *   TRUE when context has state defined or FALSE.
   */
  public function hasState(string $key): bool {
    return array_key_exists($key, $this->states);
  }

  /**
   * Sets multiple states of the context.
   *
   * @param \Drupal\contextualized_state\State\StateInterface ...$states
   *   List of states to set.
   */
  public function setStates(StateInterface ...$states): void {
    foreach ($states as $state) {
      $this->setState($state);
    }
  }

  /**
   * Sets the states of the context.
   *
   * @param \Drupal\contextualized_state\State\StateInterface $state
   *   The state.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function setState(StateInterface $state) {
    $key = $state->getKey();
    $value = $state->getValue();

    $this->states[$key] = $state;
    $this->privateTempStore->set($key, $value);
    $this->saveMyKeys($key);
  }

  /**
   * Gets a given state.
   *
   * @param string $key
   *   The state key.
   *
   * @return \Drupal\contextualized_state\State\StateInterface
   *   The state or null.
   */
  public function getState(string $key): StateInterface {
    if (array_key_exists($key, $this->states)) {
      return $this->states[$key];
    }

    if ($stateValue = $this->getStateFromStore($key)) {
      return State::create($key, $stateValue);
    }

    return NullState::create('', '');
  }

  /**
   * The state from private store.
   *
   * @param string $key
   *   The state key.
   *
   * @return mixed
   *   The state value.
   */
  public function getStateFromStore(string $key) {
    return $this->privateTempStore->get($key);
  }

  /**
   * Sets state by value.
   *
   * @param string $key
   *   The key state.
   * @param string $value
   *   The value state.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function setStateValue(string $key, string $value) {
    $this->setState(State::create($key, $value));
  }

  /**
   * Sets whole context of the class.
   *
   * @param array $context
   *   The context array data.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function setContext(array $context = []): void {
    if (empty($context)) {
      return;
    }

    foreach ($context as $key => $value) {
      $this->setStateValue($key, $value);
    }
  }

  /**
   * Save my keys.
   *
   * @param string $key
   *   The key.
   */
  private function saveMyKeys(string $key) {
    $keys = $this->privateTempStore->get('my_keys');

    if ($keys && in_array($key, $keys)) {
      return;
    }

    if (!$keys) {
      $this->privateTempStore->set('my_keys', [$key]);

      return;
    }

    $keys[] = $key;
    $this->privateTempStore->set('my_keys', $keys);
  }

  /**
   * Get all the elements from storage.
   *
   * @return array
   *   The element's storage.
   */
  public function getAll(): array {
    $keys = $this->privateTempStore->get('my_keys');

    if (empty($keys)) {
      return [];
    }

    $values = [];

    foreach ($keys as $key) {
      $values[$key] = $this->getStateFromStore($key);
    }

    return $values;
  }

  /**
   * Gets the cache tags defined for the context.
   *
   * @return string[]
   *   The cache tag list.
   */
  public function getCacheTags(): array {
    return ["contextualized_state"];
  }

}
