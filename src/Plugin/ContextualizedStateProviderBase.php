<?php

namespace Drupal\contextualized_state\Plugin;

use Drupal\contextualized_state\Context\BaseContext;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Context Provider plugins.
 */
abstract class ContextualizedStateProviderBase extends PluginBase implements ContextualizedStateProviderInterface, ContainerFactoryPluginInterface {

  /**
   * Initializes the context object.
   *
   * @param array $context
   *   The context data.
   */
  abstract public function initializeContext(array $context = []);

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function supportedTypes(): ?array {
    // Retrieve the @supported_types property from the annotation
    // and return it.
    return $this->pluginDefinition['supported_types'];
  }

  /**
   * Checks whether the type is supported or not.
   *
   * @param string $type
   *   The type.
   *
   * @return bool
   *   Returns TRUE when the type is supported.
   */
  public function isTypeSupported(string $type): bool {
    $ignoreIt = empty($this->supportedTypes()) || empty($type);
    if ($ignoreIt) {
      return TRUE;
    }

    return in_array($type, $this->supportedTypes());
  }

  /**
   * The context.
   *
   * @var \Drupal\contextualized_state\Context\BaseContext
   */
  private BaseContext $context;

  /**
   * Sets the context.
   *
   * @param \Drupal\contextualized_state\Context\BaseContext $context
   *   The context class.
   */
  public function setContext(BaseContext $context) {
    $this->context = $context;
  }

  /**
   * Gets the context.
   *
   * @return \Drupal\contextualized_state\Context\BaseContext
   *   The current context.
   */
  public function getContext(): BaseContext {
    return $this->context;
  }

}
