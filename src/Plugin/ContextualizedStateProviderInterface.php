<?php

namespace Drupal\contextualized_state\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Context Provider plugins.
 */
interface ContextualizedStateProviderInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.
}
