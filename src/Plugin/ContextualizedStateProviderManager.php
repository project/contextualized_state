<?php

namespace Drupal\contextualized_state\Plugin;

use Drupal\contextualized_state\Annotation\ContextualizedStateProvider;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Context Provider plugin manager.
 */
class ContextualizedStateProviderManager extends DefaultPluginManager {

  /**
   * Constructs a new ContextualizedStateProviderManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ContextualizedState', $namespaces, $module_handler, ContextualizedStateProviderInterface::class, ContextualizedStateProvider::class);

    $this->alterInfo('contextualized_state_contextualized_state_provider_info');
    $this->setCacheBackend($cache_backend, 'contextualized_state_contextualized_state_provider_plugins');
  }

}
