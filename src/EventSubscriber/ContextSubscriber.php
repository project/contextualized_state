<?php

namespace Drupal\contextualized_state\EventSubscriber;

use Drupal\contextualized_state\Event\ContextEvent;
use Drupal\contextualized_state\Service\ContextualizedStateManager;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber subscribing to context actions.
 */
class ContextSubscriber implements EventSubscriberInterface {

  /**
   * The context manager.
   *
   * @var \Drupal\contextualized_state\Service\ContextualizedStateManager
   */
  protected ContextualizedStateManager $contextManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The service constructor.
   *
   * @param \Drupal\contextualized_state\Service\ContextualizedStateManager $contextManager
   *   The context manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler interface.
   */
  public function __construct(
    ContextualizedStateManager $contextManager,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->contextManager = $contextManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ContextEvent::ON_SET_CONTEXT => 'onSetContext',
    ];
  }

  /**
   * Sets the context based on the event.
   *
   * @param \Drupal\contextualized_state\Event\ContextEvent $event
   *   The event action.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function onSetContext(ContextEvent $event) {
    $eventContextData = $event->context;

    $this->moduleHandler
      ->alter(['contextualized_state', 'contextualized_state_' . $eventContextData['type']], $event);

    $context = $this->contextManager->contextResolver($eventContextData);

    if (!$context) {
      return;
    }

    $context->setContext($event->context);

    Cache::invalidateTags($context->getCacheTags());
  }

}
