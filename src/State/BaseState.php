<?php

namespace Drupal\contextualized_state\State;

/**
 * The base state class.
 */
abstract class BaseState implements StateInterface {

  /**
   * The key value.
   *
   * @var string
   */
  private string $key;

  /**
   * The value state.
   *
   * @var mixed
   */
  private $value;

  /**
   * The construct class.
   *
   * @param string $key
   *   The key state.
   * @param string $value
   *   The value state.
   */
  public function __construct(string $key, $value) {
    $this->key = $key;
    $this->value = $value;
  }

  /**
   * Get key.
   */
  public function getKey(): string {
    return $this->key;
  }

  /**
   * Get value.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Creates new state.
   *
   * @param string $key
   *   The key state.
   * @param string $value
   *   The value state.
   *
   * @return $this
   *   New state created.
   */
  public static function create(string $key, $value): BaseState {
    return new static($key, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function __toString(): string {
    return $this->getValue();
  }

}
