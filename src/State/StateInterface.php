<?php

namespace Drupal\contextualized_state\State;

/**
 * States are meant to be a piece of value of the context.
 */
interface StateInterface {

  /**
   * Gets the key of the state.
   *
   * @return string
   *   The state key.
   */
  public function getKey(): string;

  /**
   * Gets the value of the state.
   *
   * @return mixed
   *   The state value.
   */
  public function getValue();

}
