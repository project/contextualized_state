<?php

namespace Drupal\contextualized_state\State;

/**
 * The state class.
 */
final class NullState extends BaseState {}
